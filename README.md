# Android Task Injection POC
Task Hijacking in Android (somebody call it also StrandHogg)

More details:

- https://www.usenix.org/system/files/conference/usenixsecurity15/sec15-paper-ren-chuangang.pdf
- https://twitter.com/ivanmarkovicsec/status/1201592031333761024
- https://promon.co/security-news/strandhogg/
